# 분류에서 불균형 데이터 처리 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 기법을 사용하여 분류 작업에서 불균형한 데이터를 처리하는 방법을 알아본다.</font>

## 목차

1. [개요](./handling-imbalanced-data.md#intro)
1. [불균형 데이터란, 무엇이 문제인가?](./handling-imbalanced-data.md#sec_02)
1. [불균형과 성능 측정 방법](./handling-imbalanced-data.md#sec_03)
1. [불균형 데이터 처리 기법](./handling-imbalanced-data.md#sec_04)
1. [Cost Sensitive Training의 구현 예](./handling-imbalanced-data.md#sec_05)
1. [마치며](./handling-imbalanced-data.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 22 — Handling Imbalanced Data in Classification](https://ai.plainenglish.io/ml-tutorial-22-handling-imbalanced-data-in-classification-7a7b79db0d93?sk=d589b4fb8a570eb1717945cdb48935af)를 편역하였습니다.
