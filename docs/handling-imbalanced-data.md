# 분류에서 불균형 데이터 처리

## <a name="intro"></a> 개요
이 포스팅에서는 다양한 기법을 사용하여 분류 작업에서 불균형 데이터를 처리하는 방법을 설명한다. 불균형 데이터는 기계 학습, 특히 각 클래스의 인스턴스 수가 동일하지 않은 분류에서 흔히 발생하는 문제이다. 예를 들어 신용카드 거래를 부정 거래 또는 합법적 거래로 분류하고자 한다고 가정해보자. 대부분의 거래는 합법적일 가능성이 높은 반면, 극히 일부만 부정 거래이다. 이는 데이터에 불균형을 발생시켜 분류기의 성능과 정확성에 영향을 미칠 수 있다.

불균형 데이터가 문제가 되는 이유는 무엇일까? 여러분은 불균형 데이터와 분류기의 성능을 어떻게 측정할 수 있을까? 당신은 불균형 데이터를 다루기 위해 어떤 기술을 사용할 수 있는가? 이것들은 우리가 이 포스팅에서 제기하는 질문들이다. 이 포스팅에 대한 학습이 끝나면 다음을 할 수 있기를 기대한다.

- 분류에서 불균형 데이터의 개념과 함의를 이해한다.
- 다양한 메트릭과 기법을 사용하여 분류기의 불균형과 성능을 측정한다.
- 오버샘플링, 언더샘플링, 합성 데이터 생성 등 다양한 기법을 적용하여 불균형 데이터를 처리한다.
- scikit-learn 및 imbalanced-learn 라이브러리를 사용하여 Python에서 이러한 기술을 구현한다.

시작합시다!

## <a name="sec_02"></a> 불균형 데이터란, 무엇이 문제인가?
불균형 데이터는 데이터세트의 각 클래스의 인스턴스 수가 동일하지 않은 경우를 말한다. 예를 들어, 1000개의 인스턴스로 구성된 데이터세트가 있다고 가정해보자. 클래스 A는 클래스 B보다 9배나 많은 인스턴스를 가지고 있기 때문에 이 데이터세트는 불균형 상태이다.

왜 불균형 데이터가 문제일까? 대부분의 기계 학습 알고리즘은 데이터가 균형을 이루고 있다고 가정하고, 전체적인 정확도를 바탕으로 성능을 최적화하기 때문이다. 즉, 다수 클래스를 선호하고 소수 클래스은 무시하는 경향이 있으며, 그 결과 소수 클래스에서는 성능이 떨어진다. 예를 들어 신용카드 거래가 사기인지 합법적인지를 예측하는 분류기를 가지고 있다고 가정해보자. 이때 데이터세트는 불균형할 것이며, 거래의 99%가 합법적이고 1%가 사기라고 가정할 때, 분류기는 모든 거래를 합법적이라고 예측하기만 하면 99%의 높은 정확도를 달성할 수 있다. 그러나 이 분류기는 가장 중요한 부정 거래를 탐지하지 못하기 때문에 아무 소용이 없다.

따라서 불균형 데이터는 분류기의 성능과 신뢰성에 영향을 미치기 때문에 분류 작업에 어려움을 준다. 다음 절에서는 다양한 메트릭과 기법을 사용하여 불균형과 분류기의 성능을 측정하는 방법을 알아본다.

## <a name="sec_03"></a> 불균형과 성능 측정 방법
이 절에서는 다양한 메트릭과 기법을 사용하여 분류기의 불균형과 성능을 측정하는 방법을 배울 것이다. 불균형과 성능을 측정하는 것은 중요하다. 이는 분류기의 품질과 신뢰성을 평가하고 불균형 데이터를 처리하는 다양한 기법을 비교하는 데 도움이 되기 때문이다.

먼저 데이터 집합의 불균형을 측정하는 방법을 알아보자. 불균형을 측정하는 한 가지 간단한 방법은 각 클래스의 인스턴스의 백분율인 **클래스 분포(class distribution)**를 계산하는 것이다. 예를 들어, 1000개의 인스턴스로 구성된 데이터세트가 있다고 가정하자. 클래스 A에 90%, 클래스 B에 10%의 인스턴스가 있다. 이는 클래스 A가 클래스 B보다 인스턴스 수가 9배나 많기 때문에 높은 불균형을 나타낸다.

불균형을 측정하는 또 다른 방법은 소수 클래스의 인스턴스 수와 다수 클래스의 인스턴스 수의 비율인 **클래스 비율(class ratio)**를 계산하는 것이다. 예를 들어, 동일한 데이터세트에서 클래스 비율은 0.11인데, 이는 클래스 B의 모든 인스턴스에 대해 클래스 A의 인스턴스가 9개라는 것을 의미한다. 이는 또한 소수 클래스가 다수 클래스보다 훨씬 작기 때문에 높은 불균형을 나타낸다.

그러나 이러한 측정은 피처의 수, 값의 분포, 클래스 간의 중복과 같은 데이터의 복잡성과 다양성을 고려하지 않는다. 따라서 분류 작업의 진정한 난이도를 반영하지 못할 수 있다. 예를 들어, 1000개의 인스턴스로 구성된 데이터세트가 있다고 가정하고, 여기서 500개는 클래스 A, 500개는 클래스 B에 속한다. 클래스 분포와 클래스 비율은 모두 0.5이며, 이는 균형 잡힌 데이터세트를 나타낸다. 그러나 클래스 A의 인스턴스가 피처 공간의 작은 영역에 클러스터링되어 있는 반면 클래스 B의 인스턴스는 전체 특징 공간에 흩어져 있는 경우, 클래스가 잘 분리되어 있지 않기 때문에 분류 작업이 여전히 어려울 수 있다.

따라서 **불균형 비율(imbalanced ratio, IR)**, **기하 평균(geometric mean, G-mean)**, **불균형 지수(index of imbalance, I)**와 같이 보다 정교한 기법을 사용하여 불균형을 측정하는 것도 유용하다. 이러한 기법은 데이터의 복잡성과 다양성을 고려하며, 불균형에 대한 보다 정확하고 포괄적인 측정을 제공한다. 이러한 기법에 대한 보다 자세한 내용을 이 논문 [Measuring the class-imbalance extent of multi-class problems](https://www.sciencedirect.com/science/article/abs/pii/S016786551730257X)에서 참조할 수 있다.

이제 데이터세트의 불균형을 측정하는 방법을 살펴보았으므로 분류기의 성능을 측정하는 방법을 살펴보자. 분류기의 성능을 측정하는 가장 일반적인 방법은 정확하게 분류된 인스턴스의 백분율인 **w정확도(accuracy)**를 사용하는 것이다. 예를 들어 신용카드 거래가 사기인지 합법적인지를 예측하는 분류기가 있다고 가정하고, 1000개의 인스턴스로 구성된 데이터세트에서 테스트했는데, 여기서 990개는 합법적이고 10개는 사기이다. 분류기가 합법적인 거래를 980개, 5개의 부정 거래를 정확하게 예측한다면 정확도는 98.5%이다. 이는 높은 정확도처럼 보이지만 정말 좋은 성능일까?

정확도가 데이터의 불균형을 고려하지 않고 소수 클래스에 대한 성능을 반영하지 않기 때문에 정답은 "아니오"이다. 실제로 정확도는 다수 계층에 의해 부풀려지고 소수 계층을 무시할 수 있기 때문에 오해의 소지가 있고 기만적일 수 있다. 예를 들어 동일한 데이터세트에서 분류기가 모든 트랜잭션을 합법적인 것으로 예측하면 정확도는 여전히 99%이지만 분류기는 부정적인 트랜잭션을 감지하지 못하기 때문에 유용하지 않다. 따라서 정확도는 분류기의 진정한 품질과 신뢰성을 포착하지 못하기 때문에 불균형 데이터에 대한 좋은 성능 척도가 되지 못한다.

따라서 데이터의 불균형을 고려하고, 다수와 소수 계층 모두의 성능을 반영하는 다른 메트릭을 사용하는 것이 좋다. 이러한 메트릭 중 일부는 **정밀도(precision)**, **재현율(recall)**, **F1-score**, **ROC 곡선(AUC) 아래의 영역** 및 **균형 정확도(balanced accuracy)**이다. 이러한 메트릭은 아래와 같이 분류기의 결과를 요약한 표인 **혼돈 행렬(confusion matrix)**을 기반으로 한다.

```
# Confusion matrix for a binary classification task
# Actual class: the true class of the instance
# Predicted class: the class predicted by the classifier
# TP: true positive, the instance is positive and is predicted as positive
# TN: true negative, the instance is negative and is predicted as negative
# FP: false positive, the instance is negative but is predicted as positive
# FN: false negative, the instance is positive but is predicted as negative
|                 | Predicted Positive | Predicted Negative |
|-----------------|--------------------|--------------------|
| Actual Positive | TP                 | FN                 |
| Actual Negative | FP                 | TN                 |
```

이러한 메트릭의 정의와 공식은 다음과 같다.

- **정밀도(Precision)**: 긍정적인 예측이 정확한 비율이다. 이것은 분류기가 긍정적인 등급을 예측할 때 얼마나 정확한지 측정한다. 공식은 다음과 같다.

$Precision = {TP \over {TP+FP}}$

- **재현율(recall)**: 정확하게 예측된 긍정적인 사례의 백분율이다. 이것은 분류기가 긍정적인 클래스에 얼마나 민감한지 측정한다. 공식은 다음과 같다.

$Recall = {TP \over {TP+FN}}$

- **F1-score**: 정밀도와 재현율의 조화평균. 정밀도와 재현율의 균형을 측정한다. 공식은 다음과 같다.

$F1-score = {{2 \times Precision \times Recall} \over {Precision + Recall}}$

- **AUC**: ROC 곡선 아래의 영역으로, 다른 임계값에 대한 참 긍정(true positive)률(재현률) 대 거짓 양성(false positive)률(1 — specificity)의 도표이다. 이는 모든 가능한 임계값에 걸쳐 분류기의 전반적인 성능을 측정한다. 공식은 다음과 같다.

$AUC = \int_0^1 TPR(FPR)dFPR$

<span style="color:red">위의 식 수정 필요</span>

- **균형 잡힌 정확도(balanced accuracy)**: 각 클래스에 대한 재현률의 평균. 다수 클래스와 소수 클래스 모두에서 분류기의 성능을 측정한다. 공식은 다음과 같다.

$Balanced accuracy = {{Recall_positive + Recall_negative} \over 2}$

이러한 메트릭은 다수 클래스와 소수 클래스 모두에서 분류기의 품질과 신뢰성을 포착하기 때문에 불균형 데이터에 더 적합하다. 예를 들어 신용카드 거래가 사기인지 합법적인지 여부를 예측하는 분류기가 있다고 가정하고, 이를 1000개 인스턴스의 데이터세트에서 테스트했다. 여기서 990개는 합법적이고 10개는 사기이다. 분류기가 합법적인 거래 980개와 부정 거래 5개를 정확하게 예측한다면 메트릭은 다음과 같다.

- 정확도: 98.5%
- 정밀도: 71.4%
- 재현률: 50%
- F1-score: 58.8%
- AUC: 0.75
- 균형 잡힌 정확도: 0.75

보다시피 정확도는 높지만 다른 메트릭들은 낮아 소수 클래스에 대한 분류기의 수행이 잘 이루어지지 않고 있음을 알 수 있다. 따라서 이러한 메트릭들은 분류기에 대한 보다 포괄적이고 현실적인 평가를 제공한다.

다음 절에서는 이러한 메트릭을 Python에서 scikit-learn과 imbalanced-learn 라이브러리를 사용하여 구현하는 방법을 살펴볼 것이다.

## <a name="sec_04"></a> 불균형 데이터 처리 기법
이 절에서는 다양한 기법을 사용하여 분류 작업에서 불균형 데이터를 처리하는 방법을 설명한다. 불균형 데이터를 처리하는 것이 중요하다. 이는 분류기의 성능과 신뢰성을 향상시키고, 다수와 소수 클래스 모두에게 더 공정하고 균형 있게 해줄 수 있기 때문이다.

불균형 데이터를 처리하기 위한 기법은 크게 **데이터 레벨**, **알고리즘 레벨**과 **하이브리드** 3가지로 구분되며, 이들 기법에 대해서는 다음과 같이 간략하게 설명할 수 있다.

- **데이터 레벨**: 이 기법은 데이터를 수정하여 소수 클래스의 인스턴스 수를 늘리거나(**oversampling**), 다수 클래스의 인스턴스 수를 줄이거나(**undersampling**), 또는 둘 다(**combination**)를 수행하여, 보다 균형 잡힌 데이터세트를 생성한다. 데이터 레벨 기법의 예로는 **random sampling**, **SMOTE**, **ADASYN**, **Tomek links**, **ENN**, **SMOTEEN** 등이 있다.
- **알고리즘 레벨**: 이 기법들은 학습 프로세스를 변경하거나(**cost-sensitive learning**), 출력 결정을 변경하거나(**threshold moving**), 또는 두 가지를 모두 수행(**ensemble learning**)함으로써 알고리즘을 소수 계층에 더 민감하게 수정한다. 알고리즘 레벨 기법의 예로는 **weighting**, **class-balancing**, **rejection sampling**, **ROC optimization**, **bagging**, **boosting**, **stacking** 등이 있다.
- **하이브리드**: 이 기술은 데이터 레벨과 알고리즘 레벨의 기술을 결합하여 보다 효과적이고 강력한 솔루션을 제공한다. 하이브리드 기술의 예로는 **SMOTEBoost**, **RUSBoost**, **EasyEnsemble**, **BalanceCascade** 등이 있다.

다음 절에서는 이러한 기법들을 Python에서 scikit-learn과 unbalanced-learn 라이브러리를 사용하여 구현하는 방법을 살펴볼 것이다.

## <a name="sec_05"></a> Cost Sensitive Training의 구현 예
cost sensitive training은 알고리즘이 다양한 종류의 오류와 관련된 다양한 비용을 고려하여 훈련되는 기계 학습 기술이다. 한 클래스의 인스턴스를 잘못 분류하는 비용은 일반적인 이진 분류 문제에서 다른 클래스의 인스턴스를 잘못 분류하는 비용과 다를 수 있다. 비용 민감 훈련의 목표는 이러한 비용을 모델-훈련 절차에 통합하는 것이다<sup>[1](#footnote_1)</sup>.

```python
import warnings

from sklearn.model_selection import train_test_split
from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.metrics import roc_auc_score, accuracy_score
from sklearn.datasets import make_classification


# Suppress FutureWarning messages
warnings.simplefilter(action='ignore', category=FutureWarning)

# Creating a synthetic imbalanced dataset
X, y = make_classification(n_classes=2, weights=[
                           0.9, 0.1], n_features=10, n_samples=1000, random_state=42)

# Splitting the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42)

# Creating and training a BalancedRandomForestClassifier
clf = BalancedRandomForestClassifier(random_state=42)
clf.fit(X_train, y_train)

# Making predictions on the test set
y_pred = clf.predict(X_test)

# Calculate ROC-AUC score
roc_auc = roc_auc_score(y_test, y_pred)
print(f"ROC-AUC Score: {roc_auc:.4f}")

# Calculate accuracy
accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy:.4f}")
```

출력:

```python
ROC-AUC Score: 0.8714
Accuracy: 0.8650
```

## <a name="summary"></a> 마치며
이 포스팅에서는 다양한 기법을 사용하여 분류 작업에서 불균형 데이터를 처리하는 방법을 설명하였다. 다양한 메트릭과 기법을 사용하여 불균형 데이터와 분류기의 성능을 측정하는 방법, scikit-learn과 불균형 학습 라이브러리를 사용하여 Python으로 구현하는 방법을 살펴보았다(?). 불균형 데이터를 처리하기 위해 oversampling, undersampling, synthetic data generation, cost-sensitive learning, threshold moving, and ensemble learning 등 다양한 기법을 적용하는 방법과 scikit-learn과 unbalanced-learn 라이브러리를 사용하여 Python으로 구현하는 방법을 살펴보았다.

기계 학습, 특히 각 클래스의 인스턴스 수가 동일하지 않은 분류에서 불균형 데이터를 다루는 것은 중요하고 어려운 문제이다. 이 포스팅에서 설명한 기술을 적용하면 분류기의 성능과 신뢰성을 향상시킬 수 있고, 다수 클래스와 소수 클래스 모두에게 더 공정하고 균형 잡힌 데이터를 만들 수 있다.

<a name="footnote_1">1</a>: [How to Handle Imbalanced Classes in Machine Learning](https://www.geeksforgeeks.org/how-to-handle-imbalanced-classes-in-machine-learning/)
